import React from "react";
import Tree from "react-d3-tree";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import Table from "@material-ui/core/Table";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MenuItem from "@material-ui/core/MenuItem";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import DialogTitle from "@material-ui/core/DialogTitle";
import CardContent from "@material-ui/core/CardContent";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

interface RawNodeDatum {
  name: string;
  attributes?: Record<string, string>;
  children?: RawNodeDatum[];
}

const RELATIONSHIPS = [
  { name: "wife", type: "female" },
  { name: "husband", type: "male" },
  { name: "father", type: "male" },
  { name: "mother", type: "female" },
  { name: "son", type: "male" },
  { name: "daughter", type: "female" },
];

export default function OrgChartTree() {
  //   const orgChart;
  const [tree, setTree] = React.useState<RawNodeDatum>();
  const [open, setOpen] = React.useState(false);
  const [formValues, setFormValues] = React.useState<{
    name: string;
    amount: number;
    relationShip: string;
    gender: string;
  }>({ gender: "male", amount: 0, name: "", relationShip: "" });
  const [inheritanceShare, setInheritanceShare] = React.useState([
    { relation: "", percentage: "" },
  ]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const addNode = () => {
    if (tree) {
      console.log("vvvvvvvvvv");
      // adding other relations
      setTree((prev) => {
        return {
          ...prev,
          children: [
            ...(prev?.children ? prev.children : []),
            {
              name: formValues.name,
              attributes: {
                relationShip: formValues.relationShip,
                gender: formValues.gender,
              },
            },
          ],
        };
      });
      handleClose();
    } else {
      // adding dead person
      setTree({
        name: formValues.name,
        attributes: {
          amount: formValues.amount.toString(),
          gender: formValues.gender,
        },
      });
    }
    setFormValues({ gender: "male", amount: 0, name: "", relationShip: "" });
    console.log("sssss");
  };

  const onNodeClick = (e: any) => {
    if (e.name === tree?.name) {
      handleClickOpen();
    }
    console.log("2222", e);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormValues((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const calculateShare = () => {
    console.log("treee", tree);
    if (tree) {
      console.log("xxxxxxxxxxx", {
        name: tree.name,
        money: Number(tree.attributes?.amount),
        isMale: tree.attributes?.gender === "male" ? true : false,
        relations:
          tree.children?.map((rel) => ({
            name: rel.name,
            relation: rel.attributes?.relationShip,
          })) || [],
      });
      fetch("https://family-tree-apis.herokuapp.com/createFamilyTree", {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        headers: {
          "Content-Type": "application/json",
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: JSON.stringify({
          name: tree.name,
          money: Number(tree.attributes?.amount),
          isMale: tree.attributes?.gender === "male" ? true : false,
          relations:
            tree.children?.map((rel) => ({
              name: rel.name,
              relation: rel.attributes?.relationShip,
              money: 0,
            })) || [],
        }), // body data type must match "Content-Type" header
      })
        .then((res) => res.json())
        .then((res) => {
          console.log("res", res);
          fetch("https://family-tree-apis.herokuapp.com/computeLegacy", {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: {
              "Content-Type": "application/json",
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: JSON.stringify({
              _id: res._id,
            }), // body data type must match "Content-Type" header
          })
            .then((res) => res.json())
            .then((res) => {
              console.log("resssssssssssssssss", res);
              setInheritanceShare(
                res.result.map((record: any) => ({
                  relation: record.relation,
                  percentage: record.percentage,
                }))
              );
            }); // body data type must match "Content-Type" header
        })
        .catch((e) => console.log("ee", e));
    }
  };

  return (
    <Container maxWidth="lg">
      <Box display="flex" flexDirection="column">
        <div
          id="treeWrapper"
          style={{
            width: "100%",
            height: "450px",
          }}
        >
          {tree && (
            <Tree
              data={tree}
              translate={{ x: 500, y: 100 }}
              onNodeClick={onNodeClick}
              collapsible={false}
              zoomable={false}
              pathFunc="diagonal"
              orientation="vertical"
            />
          )}
        </div>
        <Dialog
          open={open || !tree}
          onClose={handleClose}
          aria-labelledby="form-dialog-title"
          maxWidth="xs"
          fullWidth
        >
          <DialogTitle id="form-dialog-title">
            {tree ? "Relative Info" : "Dead Person Info"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              {tree
                ? "Add Relative Info"
                : "To Initialize the tree. You have to start from the dead person"}
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              name="name"
              label="Name"
              fullWidth
              value={formValues.name}
              onChange={handleChange}
            />
            {!tree && (
              <TextField
                autoFocus
                margin="dense"
                type="number"
                name="amount"
                id="amount"
                label="amount"
                fullWidth
                value={formValues.amount}
                onChange={handleChange}
              />
            )}
            <TextField
              autoFocus
              margin="dense"
              name="gender"
              id="gender"
              label="gender"
              fullWidth
              value={formValues.gender}
              onChange={handleChange}
              select
            >
              [
              <MenuItem key={"female"} value={"female"}>
                female
              </MenuItem>
              <MenuItem key={"male"} value={"male"}>
                male
              </MenuItem>
              ]
            </TextField>
            {tree && (
              <TextField
                autoFocus
                margin="dense"
                name="relationShip"
                id="relationship"
                label="Relationship"
                fullWidth
                value={formValues.relationShip}
                onChange={handleChange}
                select
              >
                {RELATIONSHIPS.filter(({ type }) => type === formValues.gender)
                  .filter((rel, index) => {
                    const valid =
                      (tree.attributes?.gender === formValues.gender &&
                        index !== 0) ||
                      tree.attributes?.gender !== formValues.gender;
                    return valid;
                  })
                  .map((relation) => {
                    return (
                      <MenuItem key={relation.name} value={relation.name}>
                        {relation.name}
                      </MenuItem>
                    );
                  })}
              </TextField>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={addNode} color="primary">
              {tree ? "Add Relative" : "Add Dead Person"}
            </Button>
          </DialogActions>
        </Dialog>
        {tree && (
          <Box my={3} display="flex">
            <Button
              variant="contained"
              color="primary"
              disableElevation
              onClick={calculateShare}
            >
              Calculate Inheritance Share
            </Button>
            {/* <Box mx={3}>
              <Button variant="contained" color="primary" disableElevation>
                Get Relation
              </Button>
            </Box> */}
          </Box>
        )}
        {inheritanceShare.length > 0 && (
          <Box width="50%">
            <Card style={{ backgroundColor: "#eee" }}>
              <CardContent>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell style={{ fontWeight: "bold" }} variant="head">
                        Name
                      </TableCell>
                      <TableCell
                        style={{ fontWeight: "bold" }}
                        align="right"
                        variant="head"
                      >
                        Percentage
                      </TableCell>
                      <TableCell
                        style={{ fontWeight: "bold" }}
                        align="right"
                        variant="head"
                      >
                        Amount
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {inheritanceShare?.map((relative) => (
                      <TableRow key={relative.relation}>
                        <TableCell component="th" scope="row">
                          {relative.relation}
                        </TableCell>
                        <TableCell align="right">
                          {relative.percentage}%
                        </TableCell>
                        <TableCell align="right">
                          {(Number(relative.percentage) / 100) *
                            Number(tree?.attributes?.amount || 0)}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </CardContent>
            </Card>
          </Box>
        )}
      </Box>
    </Container>
  );
}
